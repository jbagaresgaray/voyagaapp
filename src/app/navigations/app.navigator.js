import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Screen1 from '../scenes/Main/YourWorld';
import Screen2 from '../scenes/Main/Wishlist';
import Screen3 from '../scenes/Main/YourVoyages';
import Screen4 from '../scenes/Main/Friends';
import Screen5 from '../scenes/Main/Account';

const Tab = createBottomTabNavigator();

const AppNavigator = () => {
  return (
    <Tab.Navigator initialRouteName="YourWorld">
      <Tab.Screen
        name="YourWorld"
        component={Screen1}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({color, size}) => (
            <FontAwesome5 name="plane-departure" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Wishlist"
        component={Screen2}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({color, size}) => (
            <FontAwesome5 name="heart" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="YourVoyages"
        component={Screen3}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="airplane" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="TravellingFriends"
        component={Screen4}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({color, size}) => (
            <FontAwesome5 name="users" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="YourAccount"
        component={Screen5}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({color, size}) => (
            <FontAwesome5 name="user-alt" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default AppNavigator;
