import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

import AppNavigator from './app.navigator';

import globals from '../../styles/Global';

import LoginScreen from '../scenes/Auth/Login';
import RegisterScreen from '../scenes/Auth/Register';
import EmailVerificationScreen from '../scenes/Auth/EmailVerification';
import CompleteProfileScreen from '../scenes/Auth/CompleteProfile';
import ThankYouScreen from '../scenes/Auth/ThankYou';
import ResetPasswordScreen from '../scenes/Auth/ResetPassword';
import ForgotPasswordScreen from '../scenes/Auth/ForgotPassword';

import AllowNotificationScreen from '../scenes/Permissions/Notifications';
import AllowLocationScreen from '../scenes/Permissions/Location';

const AuthNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          headerBackTitle: '',
          headerBackTitleVisible: false,
          headerStyle: {...globals.viewHeaderColor},
        }}>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={{
            headerTitle: '',
            headerTintColor: '#BBBBBB',
          }}
        />
        <Stack.Screen
          name="EmailVerification"
          component={EmailVerificationScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CompleteProfile"
          component={CompleteProfileScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ThankYou"
          component={ThankYouScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ForgotPassword"
          component={ForgotPasswordScreen}
          options={{
            headerTitle: '',
            headerTintColor: '#BBBBBB',
          }}
        />
        <Stack.Screen
          name="ResetPassword"
          component={ResetPasswordScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AllowNotification"
          component={AllowNotificationScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AllowLocation"
          component={AllowLocationScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Home"
          component={AppNavigator}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AuthNavigator;
