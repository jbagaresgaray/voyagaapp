import React, {useState} from 'react';
import {Input} from 'native-base';
import {StyleSheet, View} from 'react-native';

import {Colors} from '../../../../styles/Variables';
import {FONT_BOLD} from '../../../../styles/Typography';

const styles = StyleSheet.create({
  InputCodeView: {
    marginLeft: 16,
    marginBottom: 16,
  },
  InputCode: {
    width: 56,
    height: 56,
    borderRadius: 4,
    textAlign: 'center',
    color: '#5E6870',
    backgroundColor: '#E2E3E4',
    fontSize: 24,
    letterSpacing: 0,
    lineHeight: 29,
    ...FONT_BOLD,
  },
  FormInput: {
    borderWidth: 0,
  },
  FormInputActive: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#F96560',
  },
});

const VoyInputCode = props => {
  const [isFocused, setIsFocused] = useState(false);

  return (
    <View style={styles.InputCodeView} onPress={props.onPress}>
      <Input
        {...props}
        style={[
          props.style,
          styles.InputCode,
          isFocused ? styles.FormInputActive : styles.FormInput,
        ]}
        keyboardType="number-pad"
        maxLength={1}
        secureTextEntry={true}
        autoCorrect={false}
        // editable={false}
        value={props.text}
        placeholderTextColor={Colors.colorBackButton}
        onBlur={() => setIsFocused(false)}
        onFocus={() => setIsFocused(true)}
      />
    </View>
  );
};

export default VoyInputCode;
