import React, {useState} from 'react';
import {Input} from 'native-base';
import {StyleSheet} from 'react-native';

import {Colors} from '../../../../styles/Variables';
import {FONT_MEDIUM} from '../../../../styles/Typography';

const styles = StyleSheet.create({
  TextInput: {
    marginTop: 25,
    borderBottomWidth: 2,
    color: Colors.colorStandard,
    fontSize: 16,
    letterSpacing: 0.45,
    lineHeight: 24,
    ...FONT_MEDIUM,
  },
  FormInput: {
    borderBottomColor: Colors.colorInputDefault,
  },
  FormInputActive: {
    borderBottomColor: Colors.colorRedOrange,
  },
});

const VoyInput = props => {
  const [isFocused, setIsFocused] = useState(false);

  return (
    <Input
      {...props}
      style={[
        props.style,
        styles.TextInput,
        isFocused ? styles.FormInputActive : styles.FormInput,
      ]}
      placeholderTextColor={Colors.colorBackButton}
      onBlur={() => setIsFocused(false)}
      onFocus={() => setIsFocused(true)}
    />
  );
};

export default VoyInput;
