import React, {useState} from 'react';
import {Textarea} from 'native-base';
import {StyleSheet} from 'react-native';

import {Colors} from '../../../../styles/Variables';
import {FONT_MEDIUM} from '../../../../styles/Typography';

const styles = StyleSheet.create({
  Textarea: {
    marginTop: 25,
    flex: 1,
  },
  FormInput: {
    borderBottomWidth: 2,
    borderBottomColor: Colors.colorInputDefault,
    color: Colors.colorStandard,
    fontSize: 16,
    letterSpacing: 0.45,
    lineHeight: 24,
    ...FONT_MEDIUM,
  },
  FormInputActive: {
    borderBottomWidth: 2,
    borderBottomColor: Colors.colorRedOrange,
    color: Colors.colorStandard,
    fontSize: 16,
    letterSpacing: 0.45,
    lineHeight: 24,
    ...FONT_MEDIUM,
  },
});

const VoyTextArea = props => {
  const [isFocused, setIsFocused] = useState(false);

  return (
    <Textarea
      {...props}
      style={[
        props.style,
        styles.Textarea,
        isFocused ? styles.FormInputActive : styles.FormInput,
      ]}
      rowSpan={3}
      placeholderTextColor={Colors.colorBackButton}
      onBlur={() => setIsFocused(false)}
      onFocus={() => setIsFocused(true)}
    />
  );
};

export default VoyTextArea;
