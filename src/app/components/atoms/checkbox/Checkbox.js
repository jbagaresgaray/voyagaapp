import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

import {Colors} from '../../../../styles/Variables';

const styles = StyleSheet.create({
  checkbox: {
    height: 24,
    width: 24,
    fontSize: 24,
  },
});

const VoyCheckbox = props => {
  const [isSelected, setSelection] = useState(false);

  return (
    <CheckBox
      {...props}
      checked={isSelected}
      boxType={'square'}
      onValueChange={() => setSelection(!isSelected)}
      hideBox={false}
      tintColor={Colors.colorBackButton}
      onCheckColor={Colors.colorWhite}
      onFillColor={'#F4327F'}
      onTintColor={'#D23078'}
      animationDuration={0.5}
      disabled={false}
      onAnimationType={'bounce'}
      style={styles.checkbox}
    />
  );
};

export default VoyCheckbox;
