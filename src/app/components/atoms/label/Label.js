import React from 'react';
import {Text} from 'native-base';
import {StyleSheet} from 'react-native';

const VoyText = () => {
  return <Text style={styles.baseText} />;
};

const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default VoyText;
