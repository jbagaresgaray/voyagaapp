import React from 'react';
import {StyleSheet} from 'react-native';

import {Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

import {FONT_BOLD} from '../../../../styles/Typography';
// import {Colors} from '../../../../styles/Variables';

const styles = StyleSheet.create({
  buttonText: {
    ...FONT_BOLD,
    fontSize: 16,
    lineHeight: 19,
    letterSpacing: 1,
    textTransform: 'uppercase',
  },
  buttonTextTransparent: {
    ...FONT_BOLD,
    fontSize: 16,
    lineHeight: 19,
    letterSpacing: 1,
    color: '#81888E',
    textTransform: 'uppercase',
  },
  defaultButton: {
    backgroundColor: '#F4327F',
    height: 53,
  },
  buttonRounded: {
    borderRadius: 26.5,
  },
  buttonSmall: {
    height: 30,
  },
  buttonSmallText: {
    fontSize: 14,
  },
  buttonLarge: {
    height: 60,
  },
  buttonLargeText: {
    fontSize: 22,
  },
});

const VoyButton = ({
  text,
  style,
  expand,
  fill,
  shape,
  size,
  disabled,
  onPress,
}) => {
  // const buttonProps = {
  //   block: expand === 'block',
  //   full: expand === 'full',
  //   transparent: fill === 'clear',
  //   bordered: fill === 'outline',
  //   rounded: shape === 'rounded',
  //   small: size === 'small',
  //   large: size === 'large',
  // };

  const buttonFill = () => {
    if (fill === 'outline') {
      return 'outline';
    } else if (fill === 'clear') {
      return 'clear';
    } else {
      return 'solid';
    }
  };

  const buttonProps = {
    buttonStyle: [
      shape === 'rounded' ? styles.buttonRounded : undefined,
      size === 'small' ? styles.buttonSmall : undefined,
      size === 'large' ? styles.buttonLarge : undefined,
      style,
      fill !== 'clear' ? styles.defaultButton : undefined,
    ],
    onPress: onPress,
    title: text,
    disabled: disabled,
    type: buttonFill(),
    titleStyle: [
      fill === 'clear' ? styles.buttonTextTransparent : styles.buttonText,
      size === 'small' ? styles.buttonSmallText : styles.buttonText,
      size === 'large' ? styles.buttonLargeText : styles.buttonText,
    ],
  };

  if (fill === 'clear') {
    return <Button {...buttonProps} />;
  } else {
    return (
      <Button
        ViewComponent={LinearGradient}
        linearGradientProps={{
          colors: ['#D23078', '#FE6161', '#FF7955'],
          locations: [0, 0.7256, 1],
        }}
        {...buttonProps}
      />
    );
  }
};

export default VoyButton;
