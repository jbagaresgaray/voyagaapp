import React from 'react';
import {StyleSheet, View, ImageBackground, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Button, Icon} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';

import {Colors} from '../../../../styles/Variables';
import {FONT_SEMI_BOLD, FONT_MEDIUM} from '../../../../styles/Typography';

const styles = StyleSheet.create({
  voyCard: {
    height: 242,
    width: null,
    borderRadius: 16,
    position: 'relative',
    paddingBottom: 16,
  },
  voyCardHeader: {
    flex: 0,
    position: 'absolute',
    top: 20,
    left: 20,
  },
  cardShadow: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: 100,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    borderTopStartRadius: 16,
    borderTopEndRadius: 16,
  },
  voyCardSubtitle: {
    fontSize: 12,
    lineHeight: 15,
    letterSpacing: 0.33,
    color: Colors.colorBackButton,
    textTransform: 'uppercase',
    textAlign: 'left',
    ...FONT_SEMI_BOLD,
  },
  voyCardTitle: {
    fontSize: 24,
    lineHeight: 29,
    letterSpacing: 0.67,
    color: Colors.colorWhite,
    marginTop: 6,
    textAlign: 'left',
    ...FONT_MEDIUM,
  },
  voyCardHeaderButtonView: {
    flex: 0,
    marginTop: 16,
    alignSelf: 'flex-start',
  },
  voyCardHeaderButton: {
    flex: 0,
    backgroundColor: Colors.colorContent,
    // width: 78,
  },
  voyCardHeaderButtonText: {
    color: Colors.colorStandard,
    fontSize: 10,
    lineHeight: 13,
    letterSpacing: 0.58,
    ...FONT_MEDIUM,
    paddingLeft: 10,
  },
  voyCardHeaderIcon: {
    color: Colors.colorPink,
    fontSize: 12,
    marginLeft: 6,
  },
  voyCardImage: {
    flex: 1,
    position: 'relative',
  },
  voyCardImageStyle: {
    borderRadius: 16,
  },
});

const VoyCardImage = props => {
  return (
    <TouchableOpacity style={[styles.voyCard, props.style]}>
      <ImageBackground
        source={props.source}
        style={styles.voyCardImage}
        imageStyle={styles.voyCardImageStyle}
        resizeMode="cover">
        <LinearGradient
          style={styles.cardShadow}
          colors={['black', 'transparent']}
          start={{x: 0, y: 0}}
          end={{x: 0, y: 1}}
        />
        <View style={styles.voyCardHeader}>
          <Text style={styles.voyCardSubtitle}>{props.subtitle}</Text>
          <Text style={styles.voyCardTitle}>{props.title}</Text>
          <View style={styles.voyCardHeaderButtonView}>
            <Button rounded small iconRight style={styles.voyCardHeaderButton}>
              <Text style={styles.voyCardHeaderButtonText}>
                {props.buttonText}
              </Text>
              <Icon name="arrow-forward" style={styles.voyCardHeaderIcon} />
            </Button>
          </View>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default VoyCardImage;
