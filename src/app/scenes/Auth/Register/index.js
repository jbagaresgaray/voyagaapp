import React, {useState} from 'react';
import {
  Container,
  Content,
  View,
  H1,
  Form,
  Item,
  Body,
  ListItem,
} from 'native-base';
import {Text} from 'react-native-elements';

import styles from './Register.style';
import global from '../../../../styles/Global';
import VoyButton from '../../../components/atoms/button/Button';
import VoyInput from '../../../components/atoms/input/Input';
import VoyCheckbox from '../../../components/atoms/checkbox/Checkbox';

const RegistrationScreen = ({navigation}) => {
  const [isAgree, setAgreeTerms] = useState(false);

  const submitRegistration = () => {
    navigation.navigate('EmailVerification');
  };

  return (
    <Container>
      <Content style={global.viewContent}>
        <View style={styles.signInFormView}>
          <H1 style={styles.titleText}>Sign Up</H1>
          <Text style={styles.subTitleText}>
            Please fill in the information below
          </Text>

          <Form style={styles.signInForm}>
            <Item>
              <VoyInput placeholder="Name" />
            </Item>
            <Item>
              <VoyInput
                textContentType="emailAddress"
                keyboardType="email-address"
                placeholder="Email Address"
              />
            </Item>
            <Item>
              <VoyInput textContentType="password" placeholder="Password" />
            </Item>
            <Item>
              <VoyInput
                textContentType="password"
                placeholder="Confirm Password"
              />
            </Item>
            <View style={styles.signInFormAgreeView}>
              <ListItem style={styles.signInFormAgreeListItem}>
                <VoyCheckbox
                  checked={isAgree}
                  onPress={() => setAgreeTerms(!isAgree)}
                />
                <Body style={styles.signInFormAgreeBody}>
                  <Text style={styles.signInFooterLabelOne}>
                    I agree with the{' '}
                    <Text style={styles.signInFooterLabelTwo}>
                      Terms of Services
                    </Text>
                  </Text>

                  <Text style={styles.signInFooterLabelOne}>
                    {' '}
                    and{' '}
                    <Text style={styles.signInFooterLabelTwo}>
                      Privacy Policy
                    </Text>
                  </Text>
                </Body>
              </ListItem>
              {/* <VoyCheckbox /> */}
            </View>
            <View style={styles.signInFormButton}>
              <VoyButton
                expand="block"
                shape="rounded"
                text="Sign Up"
                onPress={() => {
                  submitRegistration();
                }}
              />
            </View>
            <View style={styles.signInFooter}>
              <Text style={styles.signInFooterLabelOne}>Already a member?</Text>
              <Text
                style={styles.signInFooterLabelTwo}
                onPress={navigation.goBack}>
                Sign In
              </Text>
            </View>
          </Form>
        </View>
      </Content>
    </Container>
  );
};

export default RegistrationScreen;
