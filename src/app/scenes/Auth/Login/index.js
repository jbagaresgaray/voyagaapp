import React from 'react';
import {Container, Content, Text, View, H1, Form, Item} from 'native-base';

import styles from './Login.style';
import global from '../../../../styles/Global';
import VoyButton from '../../../components/atoms/button/Button';
import VoyInput from '../../../components/atoms/input/Input';

const LoginScreen = ({navigation}) => {
  const openRegistration = () => {
    navigation.navigate('Register');
  };

  const openHome = () => {
    navigation.navigate('Home');
  };

  const forgotPassword = () => {
    navigation.navigate('ForgotPassword');
  };

  return (
    <Container>
      <Content style={global.viewContent}>
        <View style={styles.signInFormView}>
          <H1 style={styles.titleText}>Sign In</H1>
          <Text style={styles.subTitleText}>
            Enter your login details below
          </Text>

          <Form style={styles.signInForm}>
            <Item>
              <VoyInput
                textContentType="emailAddress"
                keyboardType="email-address"
                placeholder="Email Address"
              />
            </Item>
            <Item>
              <VoyInput textContentType="password" placeholder="Password" />
            </Item>
            <Text
              style={styles.forgotPassword}
              onPress={() => {
                forgotPassword();
              }}>
              Forgot Password?
            </Text>
            <View style={styles.signInFormButton}>
              <VoyButton
                expand="block"
                shape="rounded"
                text="Sign In"
                onPress={() => {
                  openHome();
                }}
              />
            </View>
            <View style={styles.signInFooter}>
              <Text style={styles.signInFooterLabelOne}>
                Don't have an Account?
              </Text>
              <Text
                style={styles.signInFooterLabelTwo}
                onPress={() => {
                  openRegistration();
                }}>
                Create Account
              </Text>
            </View>
          </Form>
        </View>
      </Content>
    </Container>
  );
};

export default LoginScreen;
