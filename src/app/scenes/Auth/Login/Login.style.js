import {StyleSheet} from 'react-native';
import {
  FONT_MEDIUM,
  FONT_LIGHT,
  FONT_SEMI_BOLD,
} from '../../../../styles/Typography';
import {Colors} from '../../../../styles/Variables';

export default StyleSheet.create({
  titleText: {
    fontSize: 24,
    lineHeight: 41,
    letterSpacing: 0.78,
    textAlign: 'left',
    ...FONT_SEMI_BOLD,
    color: Colors.colorStandard,
  },
  subTitleText: {
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.45,
    textAlign: 'left',
    ...FONT_LIGHT,
    color: Colors.colorStandardFade,
  },
  forgotPassword: {
    fontSize: 14,
    letterSpacing: 0.39,
    lineHeight: 18,
    textAlign: 'right',
    color: Colors.colorPink,
    ...FONT_MEDIUM,
    paddingTop: 25,
  },
  signInFormView: {
    marginTop: 55,
    marginStart: 20,
    marginEnd: 20,
  },
  signInForm: {
    marginTop: 88,
    marginStart: 0,
  },
  signInFormInput: {
    borderBottomWidth: 1,
    borderBottomColor: '#CCCCCC',
  },
  signInFormInputActive: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.colorRedOrange,
  },
  signInFormButton: {
    marginTop: 107,
  },
  signInFooter: {
    paddingTop: 24,
    flex: 1,
    textAlign: 'center',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  signInFooterLabelOne: {
    color: Colors.colorPink,
    fontSize: 14,
    letterSpacing: 0,
    lineHeight: 17,
    marginRight: 3,
    ...FONT_LIGHT,
  },
  signInFooterLabelTwo: {
    color: Colors.colorStandard,
    fontSize: 14,
    letterSpacing: 0,
    lineHeight: 17,
    ...FONT_SEMI_BOLD,
  },
});
