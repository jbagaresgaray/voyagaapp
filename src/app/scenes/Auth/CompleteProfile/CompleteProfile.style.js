import {StyleSheet} from 'react-native';
import {
  FONT_MEDIUM,
  FONT_BOLD,
  FONT_LIGHT,
} from '../../../../styles/Typography';
import {Colors} from '../../../../styles/Variables';

export default StyleSheet.create({
  titleText: {
    fontSize: 24,
    lineHeight: 41,
    letterSpacing: 0.78,
    textAlign: 'left',
    ...FONT_BOLD,
    color: Colors.colorStandard,
  },
  subTitleText: {
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.45,
    textAlign: 'left',
    ...FONT_LIGHT,
    color: Colors.colorStandardFade,
  },
  forgotPassword: {
    fontSize: 14,
    letterSpacing: 0.39,
    lineHeight: 18,
    textAlign: 'right',
    color: Colors.colorPink,
    ...FONT_MEDIUM,
    paddingTop: 25,
  },
  signInFormView: {
    marginTop: 15,
    marginStart: 20,
    marginEnd: 20,
  },
  signInForm: {
    marginTop: 20,
    marginStart: 0,
  },
  signInFormAgreeView: {
    marginTop: 45,
  },
  signInFormAgreeBody: {
    marginLeft: 19,
  },
  uploadPhotoView: {
    marginTop: 20,
    marginBottom: 20,
  },
  uploadPhotoText: {
    fontSize: 16,
    letterSpacing: 0.45,
    lineHeight: 19,
    color: Colors.colorStandard,
    ...FONT_MEDIUM,
  },
  uploadTextNote: {
    fontSize: 12,
    color: '#81888E',
    letterSpacing: 0.33,
    lineHeight: 14,
    ...FONT_LIGHT,
  },
  uploadPhotoAvatar: {
    height: 92,
    width: 92,
  },
  signInFormAgreeListItem: {
    borderBottomWidth: 0,
  },
  signInFormButton: {
    marginTop: 55,
  },
  signInFooter: {
    marginTop: 24,
  },
});
