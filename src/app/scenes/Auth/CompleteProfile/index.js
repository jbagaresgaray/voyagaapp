import React from 'react';
import {
  Container,
  Content,
  View,
  H1,
  Form,
  Item,
  Body,
  ListItem,
  Left,
  Thumbnail,
} from 'native-base';
import {Text} from 'react-native-elements';

import styles from './CompleteProfile.style';
import global from '../../../../styles/Global';
import VoyButton from '../../../components/atoms/button/Button';
import VoyInput from '../../../components/atoms/input/Input';
import VoyTextArea from '../../../components/atoms/input/TextArea';

const CompeleteProfileScreen = ({navigation}) => {
  const submitRegistration = () => {
    navigation.navigate('Home');
  };

  return (
    <Container>
      <Content style={global.viewContent}>
        <View style={styles.signInFormView}>
          <H1 style={styles.titleText}>Complete Profile</H1>
          <Form style={styles.signInForm}>
            <View style={styles.uploadPhotoView}>
              <ListItem avatar style={styles.signInFormAgreeListItem}>
                <Left>
                  <Thumbnail
                    style={styles.uploadPhotoAvatar}
                    source={require('../../../../assets/images/png/avatar.png')}
                  />
                </Left>
                <Body style={styles.signInFormAgreeListItem}>
                  <Text style={styles.uploadPhotoText}>Upload Photo</Text>
                  <Text note style={styles.uploadTextNote}>
                    Recommended size is 360 x360 pixels
                  </Text>
                </Body>
              </ListItem>
            </View>
            <Item>
              <VoyInput placeholder="Full name" />
            </Item>
            <Item>
              <VoyInput
                textContentType="emailAddress"
                keyboardType="email-address"
                placeholder="Email Address"
              />
            </Item>
            <Item>
              <VoyInput placeholder="Country" />
            </Item>
            <Item>
              <VoyInput placeholder="City" />
            </Item>
            <Item>
              <VoyInput placeholder="Home Airport" />
            </Item>
            <Item>
              <VoyTextArea placeholder="Short Biography" />
            </Item>
            <View style={styles.signInFormButton}>
              <VoyButton
                expand="block"
                shape="rounded"
                text="Done"
                onPress={() => {
                  submitRegistration();
                }}
              />
              <VoyButton
                text="Continue to App"
                style={styles.signInFooter}
                fill="clear"
                onPress={navigation.goBack}
              />
            </View>
          </Form>
        </View>
      </Content>
    </Container>
  );
};

export default CompeleteProfileScreen;
