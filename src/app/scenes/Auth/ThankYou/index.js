import React from 'react';
import {Container, Text, View, H1, Content} from 'native-base';

import styles from './ThankYou.style';
import global from '../../../../styles/Global';

import VoyButton from '../../../components/atoms/button/Button';
import {Image} from 'react-native';

const ThankYouScreen = ({navigation}) => {
  const submitCompleteProfile = () => {
    navigation.navigate('CompleteProfile');
  };

  const continueToApp = () => {
    navigation.navigate('AllowNotification');
  };

  return (
    <Container>
      <Content style={global.viewContent}>
        <View style={styles.viewContainer}>
          <View style={styles.viewImageContainer}>
            <Image
              source={require('../../../../assets/images/png/yourlogo.png')}
              style={styles.thankYouImage}
            />
          </View>
          <View style={styles.signInFormView}>
            <H1 style={styles.titleText}>Thank You!</H1>
            <Text style={styles.subTitleText}>
              Welcome to Voyaga we hope you enjoy all the features and have a
              wonderful experience with us
            </Text>

            <View style={styles.signInFormButton}>
              <VoyButton
                text="Complete your profile"
                expand="block"
                shape="rounded"
                onPress={() => {
                  submitCompleteProfile();
                }}
              />
              <VoyButton
                text="Continue to App"
                style={styles.signInFooter}
                fill="clear"
                onPress={() => continueToApp()}
              />
            </View>
          </View>
        </View>
      </Content>
    </Container>
  );
};

export default ThankYouScreen;
