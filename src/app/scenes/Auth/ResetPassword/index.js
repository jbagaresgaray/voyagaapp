import React from 'react';
import {
  Container,
  Content,
  Text,
  View,
  H1,
  Form,
  Item,
  Input,
} from 'native-base';

import styles from './ResetPassword.style';
import global from '../../../../styles/Global';

import VoyButton from '../../../components/atoms/button/Button';
import VoyInput from '../../../components/atoms/input/Input';

const ResetPasswordScreen = ({navigation}) => {
  const submitResetPassword = () => {};

  return (
    <Container>
      <Content style={global.viewContent}>
        <View style={styles.signInFormView}>
          <H1 style={styles.titleText}>Reset Password</H1>
          <Text style={styles.subTitleText}>
            You’ve requested a new password. Please enter your new password
            below
          </Text>

          <Form style={styles.signInForm}>
            <Item>
              <VoyInput textContentType="password" placeholder="Password" />
            </Item>
            <Item>
              <VoyInput
                textContentType="password"
                placeholder="Confirm Password"
              />
            </Item>

            <View style={styles.signInFormButton}>
              <VoyButton
                text="Submit"
                expand="block"
                shape="rounded"
                onPress={() => {
                  submitResetPassword();
                }}
              />
              <VoyButton
                text="Cancel"
                style={styles.signInFooter}
                fill="clear"
                onPress={navigation.goBack}
              />
            </View>
          </Form>
        </View>
      </Content>
    </Container>
  );
};

export default ResetPasswordScreen;
