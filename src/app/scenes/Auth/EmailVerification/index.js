import React from 'react';
import {Container, Content, Text, View, H1} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import {TextInput} from 'react-native';

import styles from './EmailVerification.style';
import global from '../../../../styles/Global';
import VoyButton from '../../../components/atoms/button/Button';
import VoyInputCode from '../../../components/atoms/input/InputCode';

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emailCode: [],
      focusPinCodeInput: true,
    };
  }

  inputRef = React.createRef();

  setEmailCode(value) {
    const pinCode = value.split('');
    this.setState({
      emailCode: pinCode,
    });
  }

  handlePinCodeFocus() {
    console.log('handlePinCodeFocus');
    this.setState({
      focusPinCodeInput: true,
    });
    this.textInput.focus();
  }

  submitResetPassword = () => {
    this.props.navigation.navigate('AllowNotification');
  };

  render() {
    return (
      <Container>
        <Content style={global.viewContent}>
          <View style={styles.signInFormView}>
            <H1 style={styles.titleText}>Email Verification</H1>
            <Text style={styles.subTitleText}>
              We’ve sent you a verification code at your email. Please check and
              enter it below
            </Text>

            <View style={styles.signInForm}>
              <Grid>
                <Col>
                  <VoyInputCode
                    text={this.state.emailCode[0]}
                    onPress={() => this.handlePinCodeFocus()}
                  />
                </Col>
                <Col>
                  <VoyInputCode
                    text={this.state.emailCode[1]}
                    onPress={() => this.handlePinCodeFocus()}
                  />
                </Col>
                <Col>
                  <VoyInputCode
                    text={this.state.emailCode[2]}
                    onPress={() => this.handlePinCodeFocus()}
                  />
                </Col>
                <Col>
                  <VoyInputCode
                    text={this.state.emailCode[3]}
                    onPress={() => this.handlePinCodeFocus()}
                  />
                </Col>
              </Grid>
              <View style={styles.signInFormButton}>
                <TextInput
                  style={styles.hiddenTextInput}
                  autoFocus={true}
                  maxLength={4}
                  keyboardType="number-pad"
                  secureTextEntry={true}
                  autoCorrect={false}
                  ref={this.inputRef}
                  onChangeText={text => this.setEmailCode(text)}
                />
                <VoyButton
                  expand="block"
                  shape="rounded"
                  text="Submit"
                  onPress={() => this.submitResetPassword()}
                />
              </View>
              <View style={styles.signInFooter}>
                <Text style={styles.signInFooterLabelOne}>
                  Didn't receive any code?
                </Text>
                <Text style={styles.signInFooterLabelTwo}>Resend Code</Text>
              </View>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
