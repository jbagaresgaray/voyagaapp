import React from 'react';
import {
  Container,
  Content,
  Text,
  View,
  H1,
  Form,
  Item,
  Input,
} from 'native-base';

import styles from './ForgotPassword.style';
import global from '../../../../styles/Global';
import VoyButton from '../../../components/atoms/button/Button';
import VoyInput from '../../../components/atoms/input/Input';

const ForgotPasswordScreen = ({navigation}) => {
  const submitForgotPassword = () => {
    navigation.navigate('ResetPassword');
  };

  return (
    <Container>
      <Content style={global.viewContent}>
        <View style={styles.signInFormView}>
          <H1 style={styles.titleText}>Forgot Password</H1>
          <Text style={styles.subTitleText}>
            Enter you email address below and we’ll send you a link to reset
            your password
          </Text>

          <Form style={styles.signInForm}>
            <Item>
              <VoyInput
                textContentType="emailAddress"
                keyboardType="email-address"
                placeholder="Email Address"
              />
            </Item>
            <View style={styles.signInFormButton}>
              <VoyButton
                text="Submit"
                expand="block"
                shape="rounded"
                onPress={() => {
                  submitForgotPassword();
                }}
              />
              <VoyButton
                text="Cancel"
                style={styles.signInFooter}
                fill="clear"
                onPress={navigation.goBack}
              />
            </View>
          </Form>
        </View>
      </Content>
    </Container>
  );
};

export default ForgotPasswordScreen;
