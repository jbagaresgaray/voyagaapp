import {StyleSheet} from 'react-native';
import {FONT_LIGHT, FONT_SEMI_BOLD} from '../../../../styles/Typography';
import {Colors} from '../../../../styles/Variables';

export default StyleSheet.create({
  titleText: {
    fontSize: 28,
    lineHeight: 41,
    letterSpacing: 0.78,
    textAlign: 'left',
    ...FONT_SEMI_BOLD,
    color: Colors.colorStandard,
  },
  subTitleText: {
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.45,
    textAlign: 'left',
    ...FONT_LIGHT,
    color: Colors.colorStandardFade,
  },
  signInFormView: {
    marginTop: 15,
    marginStart: 20,
    marginEnd: 20,
  },
  signInForm: {
    marginTop: 65,
    marginStart: 0,
  },
  signInFormButton: {
    marginTop: 107,
  },
  signInFooter: {
    paddingTop: 24,
    flex: 1,
    textAlign: 'center',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});
