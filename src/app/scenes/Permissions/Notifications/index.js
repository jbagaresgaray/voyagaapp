import React from 'react';
import {Container, Text, View, H1, Content} from 'native-base';

import styles from './Notifications.style';
import global from '../../../../styles/Global';

import VoyButton from '../../../components/atoms/button/Button';
import {Image} from 'react-native';

const AllowNotificationScreen = ({navigation}) => {
  const br = `\n`;

  const allowNotification = () => {
    navigation.navigate('AllowLocation');
  };

  const skipAllowPermision = () => {
    navigation.navigate('Home');
  };

  return (
    <Container>
      <Content style={global.viewContent}>
        <View style={styles.viewContainer}>
          <View style={styles.viewImageContainer}>
            <Image
              source={require('../../../../assets/images/png/yourlogo.png')}
              style={styles.thankYouImage}
            />
          </View>
          <View style={styles.signInFormView}>
            <H1 style={styles.titleText}>Allow {br}Notifications</H1>
            <Text style={styles.subTitleText}>
              Please enable push notification so you’ll stay updated on all
              future journeys of your friends
            </Text>

            <View style={styles.signInFormButton}>
              <VoyButton
                text="Allow"
                expand="block"
                shape="rounded"
                onPress={() => allowNotification()}
              />
              <VoyButton
                text="Maybe Later"
                style={styles.signInFooter}
                fill="clear"
                onPress={() => skipAllowPermision()}
              />
            </View>
          </View>
        </View>
      </Content>
    </Container>
  );
};

export default AllowNotificationScreen;
