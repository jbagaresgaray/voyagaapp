import React from 'react';
import {Container, Text, View, H1, Content} from 'native-base';

import styles from './Location.style';
import global from '../../../../styles/Global';

import VoyButton from '../../../components/atoms/button/Button';
import {Image} from 'react-native';

const AllowLocationScreen = ({navigation}) => {
  const br = `\n`;

  const allowNotification = () => {
    navigation.navigate('ThankYou');
  };

  const skipAllowPermision = () => {
    navigation.navigate('Home');
  };

  return (
    <Container>
      <Content style={global.viewContent}>
        <View style={styles.viewContainer}>
          <View style={styles.viewImageContainer}>
            <Image
              source={require('../../../../assets/images/png/yourlogo.png')}
              style={styles.thankYouImage}
            />
          </View>
          <View style={styles.signInFormView}>
            <H1 style={styles.titleText}>Allow access to {br}your location</H1>
            <Text style={styles.subTitleText}>
              Please allow access to location to maximize {br} the features of
              the app
            </Text>

            <View style={styles.signInFormButton}>
              <VoyButton
                text="Allow"
                expand="block"
                shape="rounded"
                onPress={() => allowNotification()}
              />
              <VoyButton
                text="Maybe Later"
                style={styles.signInFooter}
                fill="clear"
                onPress={() => skipAllowPermision()}
              />
            </View>
          </View>
        </View>
      </Content>
    </Container>
  );
};

export default AllowLocationScreen;
