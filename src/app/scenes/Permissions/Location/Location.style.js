import {StyleSheet, Dimensions} from 'react-native';
import {FONT_LIGHT, FONT_SEMI_BOLD} from '../../../../styles/Typography';
import {Colors} from '../../../../styles/Variables';

const {height} = Dimensions.get('window');

export default StyleSheet.create({
  viewContainer: {
    flex: 1,
  },
  viewImageContainer: {
    marginTop: height * 0.2,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 117,
  },
  thankYouImage: {
    height: 276,
    width: 276,
  },
  titleText: {
    fontSize: 24,
    lineHeight: 41,
    letterSpacing: 0.78,
    textAlign: 'left',
    ...FONT_SEMI_BOLD,
    color: Colors.colorStandard,
  },
  subTitleText: {
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.45,
    textAlign: 'left',
    ...FONT_LIGHT,
    color: Colors.colorStandardFade,
  },
  signInFormView: {
    marginBottom: 55,
    marginStart: 20,
    marginEnd: 20,
  },
  signInForm: {
    marginTop: 65,
    marginStart: 0,
  },
  signInFormButton: {
    marginTop: 45,
  },
  signInFooter: {
    paddingTop: 24,
    flex: 1,
    textAlign: 'center',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});
