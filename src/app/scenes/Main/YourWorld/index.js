import React from 'react';
import {Container, View, H1, Content} from 'native-base';

import styles from './YourWorld.style';
import global from '../../../../styles/Global';

import VoyCardImage from '../../../components/molecules/cards/Card';

const YourWorldScreen = () => {
  const image1 = require('../../../../assets/images/png/your_world/image1/image1.jpg');
  const image2 = require('../../../../assets/images/png/your_world/image2a/image2.png');
  const image3 = require('../../../../assets/images/png/your_world/image3/image3.jpg');

  return (
    <Container>
      <Content style={global.viewContent} padder>
        <View style={styles.yourWorldViewHeader}>
          <H1 style={styles.titleText}>Your World</H1>
        </View>
        <View style={styles.yourWorldView}>
          <VoyCardImage
            source={image1}
            subtitle="Over 3,000 Listings"
            title="All Destination"
            buttonText="Browse"
          />
          <VoyCardImage
            source={image2}
            subtitle="Over 3,000 Listings"
            title="Hot & Trending"
            buttonText="See all"
          />
          <VoyCardImage
            source={image3}
            subtitle="Over 3,000 Listings"
            title={`Most Popular in \nVoyaga`}
            buttonText="See all"
          />
        </View>
      </Content>
    </Container>
  );
};

export default YourWorldScreen;
