import {StyleSheet} from 'react-native';
import {FONT_SEMI_BOLD} from '../../../../styles/Typography';
import {Colors} from '../../../../styles/Variables';

export default StyleSheet.create({
  titleText: {
    fontSize: 28,
    lineHeight: 41,
    letterSpacing: 0.78,
    textAlign: 'left',
    ...FONT_SEMI_BOLD,
    color: Colors.colorStandard,
  },
  yourWorldViewHeader: {
    marginTop: 35,
  },
  yourWorldView: {
    marginTop: 20,
    marginStart: 0,
  },
});
