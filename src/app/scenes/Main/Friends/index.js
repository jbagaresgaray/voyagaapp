import React from 'react';
import {View, Text} from 'native-base';

const FriendsScreen = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Friends!</Text>
    </View>
  );
};

export default FriendsScreen;
