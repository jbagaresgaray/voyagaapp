import React, {useState} from 'react';
import {
  View,
  Text,
  Container,
  Content,
  H1,
  ListItem,
  List,
  Left,
  Body,
  Icon,
  Right,
} from 'native-base';
import {Avatar} from 'react-native-elements';

import styles from './Account.style';
import global from '../../../../styles/Global';
import VoyCheckbox from '../../../components/atoms/checkbox/Checkbox';

const AccountScreen = ({navigation}) => {
  const [allowNotification, setNotifications] = useState(true);

  const logoutApp = () => {
    navigation.popToTop('Home');
  };

  return (
    <Container>
      <Content style={global.viewContent}>
        <View style={styles.accountFormView}>
          <H1 style={styles.titleText}>Your Account</H1>
          <View style={styles.profileView}>
            <Avatar
              size="xlarge"
              rounded
              style={styles.profilePhotoAvatar}
              source={require('../../../../assets/images/png/avatar.png')}
            />
            <H1 style={styles.profileName}>Robert Kiyosake</H1>
            <Text style={styles.profileUploadLabel}>Upload Photo</Text>
          </View>
        </View>
        <View style={styles.settingsFormView}>
          <H1 style={styles.titleCapText}>Account Settings</H1>
          <List style={styles.settingsList}>
            <ListItem icon noBorder style={styles.settingsListItem}>
              <Left>
                <Icon
                  type="Ionicons"
                  name="md-person"
                  style={styles.settingsListItemIcon}
                />
              </Left>
              <Body style={styles.settingsFormBody}>
                <Text style={styles.settingsListItemTitle}>Profile</Text>
                <Text style={styles.settingsListItemNote}>
                  Name, email address...
                </Text>
              </Body>
              <Right>
                <Icon
                  name="arrow-forward"
                  style={styles.settingsListItemIconDetail}
                />
              </Right>
            </ListItem>
            <ListItem icon noBorder style={styles.settingsListItem}>
              <Left>
                <Icon
                  type="MaterialIcons"
                  name="stars"
                  style={styles.settingsListItemIcon}
                />
              </Left>
              <Body style={styles.settingsFormBody}>
                <Text style={styles.settingsListItemTitle}>Rate App</Text>
                <Text style={styles.settingsListItemNote}>
                  Rate app in apple store
                </Text>
              </Body>
              <Right>
                <Icon
                  name="arrow-forward"
                  style={styles.settingsListItemIconDetail}
                />
              </Right>
            </ListItem>
            <ListItem icon noBorder style={styles.settingsListItem}>
              <Left>
                <Icon
                  type="Ionicons"
                  name="md-mail"
                  style={styles.settingsListItemIcon}
                />
              </Left>
              <Body style={styles.settingsFormBody}>
                <Text style={styles.settingsListItemTitle}>Feedback</Text>
                <Text style={styles.settingsListItemNote}>
                  Send us an update
                </Text>
              </Body>
              <Right>
                <Icon
                  name="arrow-forward"
                  style={styles.settingsListItemIconDetail}
                />
              </Right>
            </ListItem>
            <ListItem icon noBorder style={styles.settingsListItem}>
              <Left>
                <Icon
                  type="Ionicons"
                  name="md-person-add"
                  style={styles.settingsListItemIcon}
                />
              </Left>
              <Body style={styles.settingsFormBody}>
                <Text style={styles.settingsListItemTitle}>Invite Friends</Text>
                <Text style={styles.settingsListItemNote}>Send invites</Text>
              </Body>
              <Right>
                <Icon
                  name="arrow-forward"
                  style={styles.settingsListItemIconDetail}
                />
              </Right>
            </ListItem>
            <ListItem noBorder>
              <Body style={styles.settingsFormBody}>
                <Text style={styles.settingsListItemTitle}>
                  Push Notifications
                </Text>
              </Body>
              <Right>
                <VoyCheckbox
                  checked={allowNotification}
                  onPress={() => setNotifications(!allowNotification)}
                />
              </Right>
            </ListItem>
            <ListItem noBorder>
              <Body style={styles.settingsFormBody}>
                <Text style={styles.settingsListItemTitle}>
                  Terms of Service
                </Text>
              </Body>
            </ListItem>
            <ListItem noBorder>
              <Body style={styles.settingsFormBody}>
                <Text style={styles.settingsListItemTitle}>Privacy Policy</Text>
              </Body>
            </ListItem>
            <ListItem noBorder onPress={() => logoutApp()}>
              <Body style={styles.settingsFormBody}>
                <Text style={styles.settingsListItemTitle}>Logout</Text>
              </Body>
            </ListItem>
          </List>
        </View>
      </Content>
    </Container>
  );
};

export default AccountScreen;
