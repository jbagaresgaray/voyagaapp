import {StyleSheet} from 'react-native';
import {
  FONT_MEDIUM,
  FONT_LIGHT,
  FONT_SEMI_BOLD,
} from '../../../../styles/Typography';
import {Colors} from '../../../../styles/Variables';

export default StyleSheet.create({
  titleText: {
    fontSize: 24,
    lineHeight: 41,
    letterSpacing: 0.78,
    textAlign: 'left',
    ...FONT_SEMI_BOLD,
    color: Colors.colorStandard,
  },
  titleCapText: {
    fontSize: 18,
    lineHeight: 22,
    letterSpacing: 0.5,
    textAlign: 'left',
    textTransform: 'uppercase',
    ...FONT_SEMI_BOLD,
    color: Colors.colorStandard,
    marginLeft: 20,
  },
  accountFormView: {
    paddingTop: 31,
    paddingStart: 20,
    paddingEnd: 20,
    marginBottom: 5,
    backgroundColor: Colors.colorContent,
    shadowColor: '#000',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 5,
  },
  settingsFormView: {
    backgroundColor: Colors.colorContent2,
    paddingTop: 40,
    paddingBottom: 40,
  },
  settingsList: {
    paddingTop: 20,
  },
  settingsListItem: {
    borderBottomWidth: 0,
    marginBottom: 30,
  },
  settingsListItemIcon: {
    fontSize: 24,
    color: Colors.colorPink2,
  },
  settingsListItemIconDetail: {
    fontSize: 24,
    color: Colors.colorDetail,
  },
  settingsListItemTitle: {
    fontSize: 16,
    lineHeight: 19,
    letterSpacing: 0.45,
    color: Colors.colorStandard,
    ...FONT_MEDIUM,
  },
  settingsListItemNote: {
    fontSize: 12,
    lineHeight: 14,
    letterSpacing: 0.33,
    color: Colors.colorNote,
    ...FONT_LIGHT,
  },
  settingsFormBody: {
    // alignSelf: 'flex-start',
  },
  profileView: {
    marginTop: 50,
    paddingBottom: 50,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  profilePhotoAvatar: {
    height: 92,
    width: 92,
    margin: 16,
  },
  profileName: {
    fontSize: 24,
    lineHeight: 29,
    letterSpacing: 0.67,
    color: Colors.colorStandard,
    ...FONT_MEDIUM,
  },
  profileUploadLabel: {
    fontSize: 12,
    lineHeight: 15,
    letterSpacing: 0.33,
    color: '#5E6870',
    marginTop: 6,
    ...FONT_MEDIUM,
  },
});
