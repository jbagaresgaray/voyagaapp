import React from 'react';
import {View, Text} from 'native-base';

const YourVoyagesScreen = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Your Voyages!</Text>
    </View>
  );
};

export default YourVoyagesScreen;
