import React from 'react';
import {Container, View, H1, Content} from 'native-base';

import styles from './Wishlist.style';
import global from '../../../../styles/Global';

import VoyCardImage from '../../../components/molecules/cards/Card';

const WishlistScreen = () => {
  const image1 = require('../../../../assets/images/png/wishlist/image1/image1.png');
  const image2 = require('../../../../assets/images/png/wishlist/image2/image2.png');
  const image3 = require('../../../../assets/images/png/wishlist/image3/image3.png');

  return (
    <Container>
      <Content style={global.viewContent} padder>
        <View style={styles.yourWorldViewHeader}>
          <H1 style={styles.titleText}>Wishlist</H1>
        </View>
        <View style={styles.yourWorldView}>
          <VoyCardImage
            style={styles.cardView}
            source={image1}
            subtitle="10 Destinations"
            title="Destinations you want to go"
            buttonText="See all"
          />
          <VoyCardImage
            style={styles.cardView}
            source={image2}
            subtitle="Top 50"
            title={`Wishlist \nRecommendation`}
            buttonText="See all"
          />
          <VoyCardImage
            style={styles.cardView}
            source={image3}
            subtitle="Top 50"
            title={`Different from where \nyou've been`}
            buttonText="See all"
          />
        </View>
      </Content>
    </Container>
  );
};

export default WishlistScreen;
