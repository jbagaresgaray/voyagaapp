import {StyleSheet} from 'react-native';
import {Colors} from './Variables';

export default StyleSheet.create({
  viewFlex: {
    flex: 1,
  },
  viewPage: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    position: 'absolute',
    flexDirection: 'column',
    justifyContent: 'space-between',
    overflow: 'hidden',
    zIndex: 0,
  },
  viewContent: {
    backgroundColor: Colors.colorContent,
  },
  viewContent2: {
    backgroundColor: Colors.colorContent2,
  },
  viewHeaderColor: {
    backgroundColor: Colors.colorContent,
    shadowColor: 'transparent',
    shadowRadius: 0,
    shadowOffset: {
      height: 0,
    },
  },
  viewBackButtonColor: {
    tintColor: Colors.colorBackButton,
  },
});
