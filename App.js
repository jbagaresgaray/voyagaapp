import 'react-native-gesture-handler';
import React from 'react';
import {Root} from 'native-base';
import Navigator from './src/app/navigations';

const App = () => (
  <Root>
    <Navigator />
  </Root>
);

export default App;
